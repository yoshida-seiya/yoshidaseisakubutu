# crowdART開発環境構築手順

### 概要
フロントエンド(利用者サイト、管理者サイト) (Nuxt.js)と   
バックエンド(APIサーバー、バッチ処理) (express.js)に分かれる。  

### フロントエンド
1.yarnをローカル(自分のPC)にインストール (v1.22.18)する。   
新しいコマンドプロンプトを起動し、`npm install -g yarn`と入力する。  
インストール完了後`yarn -v`と入力し、`1.22.18`と出力されればOK  

2.リポジトリの権限を<zhang-yijie@caica.jp>に貰う。 

3.リポジトリをローカルにcloneする。  
新しいコマンドプロンプトを起動後、`cd C:/`でCドライブに移動し  
`mkdir[任意の名前]`で任意のフォルダーを作成する。  
`cd C:￥任意のフォルダー名`で任意のフォルダまで移動する。  
`git clone [http://リポジトリのurl/プロジェクト名.git]`と入力するとgit cloneが始まる。  
任意のフォルダ内にAdmin-siteフォルダまたはGeneral-siteフォルダが作成されていればOK  
※ 管理者サイト (Admin-site) と利用者サイト (General-site) でそれぞれリポジトリが存在するので、両方ともクローンし、下記を実施する。  

4.Admin-siteフォルダにyarnを入れる。  
`cd C:￥任意のフォルダー名￥Admin-siteまたはGeneral-site`でAdmin-siteフォルダまたはGeneral-siteフォルダの最上階層に移動し、`yarn install`を実行する。  

5..envファイルを作成する。  
Admin-siteフォルダまたはGeneral-siteフォルダ内に`type nul > .env`で.envファイルを作成する。  
中身のフォーマットを<zhang-yijie@caica.jp>に聞く。  

6.起動確認をする。  
`yarn dev`を入力し、【IPアドレス】:3000で起動する。  

### バックエンド
1.Node.jsをローカル（自分のPC）にインストール (v14.15.4)する。  
[Node.js](https://nodejs.org/en/)の公式サイトにアクセスして、LTSのインストーラーをダウンロードする。  
インストール完了後`node -v`と入力し、`14.15.4`と出力されればOK  

2.npmをローカル(自分のPC)にインストール (v6.14.10)されているか確認する。  
Node.jsをインストールした時点で、自動的にnpmもインストールされているので`npm -v`と入力し、`6.14.10`と出力されればOK  

3.リポジトリの権限を<zhang-yijie@caica.jp>に貰う。 

4.リポジトリをcloneする。  
`cd C:￥任意のフォルダー名(フロントエンド3.で作成したフォルダと同じ)`で任意のフォルダまで移動する。  
`git clone [http://リポジトリのurl/プロジェクト名.git]`と入力するとgit cloneが始まる。  
任意のフォルダ内にapi-serverフォルダが作成されていればOK

5.config/default.jsonファイルを作成する。  
`cd C:￥任意のフォルダー名￥api_server`でapi-serverフォルダ内に移動し、`mkdir config`でconfigフォルダを作成する。  
`cd C:￥任意のフォルダー名￥api_server￥config`で移動し`type nul > default.json`でdefault.jsonファイルを作成する。  
default.jsonファイルの中身のフォーマットを<zhang-yijie@caica.jp>に聞く。  

6.api_serverフォルダにnpmを入れる。  
`cd C:￥任意のフォルダー名￥api_server`でapi_serverフォルダの最上階層に移動し`npm install`を実行する。  

7.起動確認をする。  
`npm start`と入力し、【IPアドレス】: 8080で起動する。 

※追加  
asw-sdkを使用するので、実行にはIAMユーザが必要なため  
平船さん<tairabune-kousuke@caica.jp>に作成を依頼する。  

今後、開発の場合はRDSを使わず、ローカル環境でDBを作成する可能性有  